/* global parse: false */

function $RootScopeProvider(){

    var TTL = 10;

    this.digestTtl = function(value){

        if(_.isNumber(value)){
            TTL = value
        }
        return TTL;
    };

    this.$get = ['$parse', function($parse){

        function Scope() {
            'use strict';

            //tableau des watchers du scope
            this.$$watchers = [];

            //Stocker le dernier watch dirty pour éviter de re-digest la boucle complète
            this.$$lastDirtyWatch = null;

            //La queue des jobs async à exécuter
            this.$$asyncQueue = [];

            //La queue pour les applyAsync
            this.$$applyAsyncQueue = [];

            //id de l'apply async timeout enregistré s'il y en a un
            this.$$applyAsyncId = null;

            //valeur de la phase courant
            this.$$phase = null;

            //fonction a déclencher en post digest
            this.$$postDigestQueue = [];

            //le scope enfants
            this.$$children = [];

            //event listener
            this.$$listeners = {};

            //le scope parent
            this.$root = this;
        }

        Scope.prototype.$new = function (isolated, parent) {
            var child;
            parent = parent || this;

            if (isolated) {
                child = new Scope();
                child.$root = parent.$root;
                child.$$asyncQueue = parent.$$asyncQueue;
                child.$$postDigestQueue = parent.$$postDigestQueue;
                child.$$applyAsyncQueue = parent.$$applyAsyncQueue;
            } else {
                var ChildScope = function () {
                };
                ChildScope.prototype = this;
                child = new ChildScope();
                child.$root = parent.$root;
            }

            //tableau des children pour le digest descendant récursif
            parent.$$children.push(child);
            // pour que le child ait son propre tableau de watcher et donc que le digest ne passe pas sur tous les watchers du parent
            child.$$watchers = [];
            child.$$children = [];
            child.$$listeners = {};
            child.$parent = parent;

            return child;
        };


        function initWatchVal() {

        }

        /**
         * exécute une fonction sur chaque scope de la hierarchie
         * @param fn Fonction à exécuter sur chaque scope
         */
        Scope.prototype.$$everyScope = function (fn) {
            if (fn(this)) {
                return this.$$children.every(function (child) {
                    return child.$$everyScope(fn);
                });
            } else {
                //cas d'arrêt
                return false;
            }
        };

        Scope.prototype.$beginPhase = function (phase) {
            if (this.$$phase) {
                throw this.$$phase + ' already in progress.';
            }

            this.$$phase = phase;
        };

        Scope.prototype.$clearPhase = function () {
            this.$$phase = null;
        };

        Scope.prototype.$$areEqual = function (newValue, oldValue, valueEq) {
            if (valueEq) {
                return _.isEqual(newValue, oldValue);
            } else {
                return newValue === oldValue || (typeof newValue === 'number' && typeof oldValue === 'number' && isNaN(newValue) && isNaN(oldValue));
            }
        };

        Scope.prototype.$apply = function (expr) {
            try {
                this.$beginPhase('$apply');
                return this.$eval(expr);
            } finally {
                this.$clearPhase();
                this.$root.$digest();
            }
        };

        Scope.prototype.$applyAsync = function (expr) {

            var self = this;

            this.$$applyAsyncQueue.push(function () {
                //pousse la fonction d'évaluation avec la référence sur le scope
                self.$eval(expr);
            });

            if (self.$root.$$applyAsyncId === null) {
                self.$root.$$applyAsyncId = setTimeout(function () {
                    self.$apply(_.bind(self.$$flushApplyAsync, self));
                }, 0);
            }

        };

        Scope.prototype.$$flushApplyAsync = function () {
            while (this.$$applyAsyncQueue.length) {
                //shift and call
                try {
                    this.$$applyAsyncQueue.shift()();
                } catch (e) {
                    console.error(e);
                }
            }
            this.$root.$$applyAsyncId = null;
        };

        Scope.prototype.$eval = function (expr, locals) {
            return $parse(expr)(this, locals);
        };

        Scope.prototype.$evalAsync = function (expr) {

            var self = this;

            //on ne planifie un digest que si et seulement si un autre evalAsync n'en a pas déjà planifié un
            if (!self.$$phase && !self.$$asyncQueue.length) {
                //On ajoute un léger délai pour avoir un retour immédiat et ainsi pouvoir stacker les evalAsync sur 1 digest
                setTimeout(function () {
                    self.$root.$digest();
                }, 0);
            }

            this.$$asyncQueue.push({scope: this, expression: expr});
        };

        Scope.prototype.$$postDigest = function (fn) {
            this.$$postDigestQueue.push(fn);
        };

        Scope.prototype.$destroy = function () {

            if (this === this.$root) {
                return;
            }

            this.$broadcast('$destroy');

            if(this.$parent){
                var siblings = this.$parent.$$children;
                var indexOfThis = siblings.indexOf(this);
                if (indexOfThis >= 0) {
                    siblings.splice(indexOfThis, 1);
                }
            }

            this.$$listeners = {};

        };

        Scope.prototype.$watch = function (watchFn, listenerFn, valueEq) {

            var self = this;

            watchFn = $parse(watchFn);

            if(watchFn.$$watchDelegate){
                return watchFn.$$watchDelegate(self, listenerFn, valueEq, watchFn);
            }

            var watcher = {
                watchFn: watchFn,
                listenerFn: listenerFn || function () {
                },
                valueEq: !!valueEq,
                last: initWatchVal
            };

            this.$$watchers.unshift(watcher);
            this.$root.$$lastDirtyWatch = null;

            //fonction de destruction du watcher
            return function () {
                var index = self.$$watchers.indexOf(watcher);

                if (index >= 0) {
                    self.$$watchers.splice(index, 1);
                    self.$root.$$lastDirtyWatch = null;
                }
            };
        };

        /**
         * Watch sur plusieurs éléments du scope. Déclenche le listener si l'un
         * des watcher détecte un changement de valeur
         * @param watchFns
         * @param listenerFn
         * @returns {Function}
         */
        Scope.prototype.$watchGroup = function (watchFns, listenerFn) {

            var self = this;

            var oldValues = new Array(watchFns.length);
            var newValues = new Array(watchFns.length);

            var changeReactionScheduled = false;
            var firstRun = true;

            var watchers = [];

            if (watchFns.length === 0) {

                var shouldCall = true;

                self.$evalAsync(function () {
                    if (shouldCall === true) {
                        listenerFn(newValues, oldValues, self);
                    }
                });
                return function () {
                    shouldCall = false;
                };
            }

            function watchGroupListener() {

                if (firstRun) {
                    firstRun = false;
                    listenerFn(newValues, newValues, self);
                } else {
                    listenerFn(newValues, oldValues, self);
                }

                changeReactionScheduled = false;
            }

            var destroyFunctions = _.map(watchFns, function (watchFn, i) {
                return self.$watch(watchFn, function (newValue, oldValue) {

                    oldValues[i] = oldValue;
                    newValues[i] = newValue;

                    if (!changeReactionScheduled) {
                        changeReactionScheduled = true;
                        self.$evalAsync(function () {
                            watchGroupListener();
                        });
                    }
                });
            });

            return function () {
                _.forEach(destroyFunctions, function (destroyFunction) {
                    destroyFunction();
                });
            };
        };

        /**
         * Top level digest
         */
        Scope.prototype.$digest = function () {
            var ttl = TTL;
            var dirty;
            this.$root.$$lastDirtyWatch = null;
            this.$beginPhase('$digest');

            //si un applyAsync a été enregistré préalablement, on cancel le timeout dans ce cylce de digest
            //et on l'exécute directement
            if (this.$root.$$applyAsyncId) {
                clearTimeout(this.$root.$$applyAsyncId);
                this.$$flushApplyAsync();
            }

            do {
                //consommation des function enregistrées dans l'asyncQueue
                while (this.$$asyncQueue.length) {
                    try {
                        var asyncTask = this.$$asyncQueue.shift();
                        asyncTask.scope.$eval(asyncTask.expression);
                    } catch (e) {
                        console.error(e);
                    }
                }

                dirty = this.$$digestOnce();

                if ((dirty || this.$$asyncQueue.length) && !(ttl--)) {
                    this.$clearPhase();
                    throw TTL + " digest iterations reached";
                }

            } while (dirty || this.$$asyncQueue.length);

            this.$clearPhase();

            //exec des fonctions post digest
            while (this.$$postDigestQueue.length) {
                try {
                    this.$$postDigestQueue.shift()();
                } catch (e) {
                    console.error(e);
                }
            }
        };


        /**
         * Internal digest
         * @returns {*}
         */
        Scope.prototype.$$digestOnce = function () {
            var self = this;
            var newValue, oldValue, dirty;
            var continueLoop = true;

            //itération de la fin vers le début pour le shift dû au watcher auto destructeur
            self.$$everyScope(function (scope) {

                _.forEachRight(scope.$$watchers, function (watcher) {

                    try {
                        if (watcher) {
                            newValue = watcher.watchFn(scope);
                            oldValue = watcher.last; //undefined the first time

                            if (!scope.$$areEqual(newValue, oldValue, watcher.valueEq)) {
                                scope.$root.$$lastDirtyWatch = watcher;
                                watcher.last = watcher.valueEq ? _.cloneDeep(newValue) : newValue; //on stocke la dernière valeur
                                //on ajoute le test sur le initWatchVal pour ne pas que la fonction initWatchVal leak du code interne
                                watcher.listenerFn(newValue, oldValue === initWatchVal ? newValue : oldValue, scope);
                                dirty = true;
                            } else if (scope.$root.$$lastDirtyWatch === watcher) {
                                continueLoop = false;
                                return false;
                            }
                        }
                    } catch (e) {
                        console.error(e);
                    }
                });
                return continueLoop;
            });

            return dirty;
        };


        Scope.prototype.$watchCollection = function (watchFn, listenerFn) {

            var self = this;
            var newValue;
            var oldValue;
            var oldLength;
            var veryOldValue;
            var trackVeryOldValue = (listenerFn.length > 1);
            var changeCount = 0;
            var firstRun = true;

            watchFn = $parse(watchFn);

            var internalWatchFn = function (scope) {
                var newLength;
                newValue = watchFn(scope);

                if (_.isObject(newValue)) {

                    if (_.isArrayLike(newValue)) {
                        if (!_.isArray(oldValue)) {
                            changeCount++;
                            oldValue = [];
                        }

                        if (newValue.length !== oldValue.length) {
                            changeCount++;
                            oldValue.length = newValue.length;
                        }

                        _.forEach(newValue, function (newItem, i) {

                            var bothNaN = typeof newItem === 'number' && typeof oldValue[i] === 'number' && isNaN(newItem) && isNaN(oldValue[i]);

                            if (newItem !== oldValue[i] && !bothNaN) {
                                changeCount++;
                                oldValue[i] = newItem;
                            }
                        });

                    } else {
                        if (!_.isObject(oldValue) && !_.isArrayLike(oldValue)) {
                            changeCount++;
                            oldValue = {};
                            oldLength = 0;
                        }

                        newLength = 0;

                        _.forOwn(newValue, function (newVal, key) {
                            newLength++;

                            //La propriété était présente dans l'ancienne objet mais a pu être modifié
                            if (oldValue.hasOwnProperty(key)) {
                                var bothNaN = typeof newVal === 'number' && typeof oldValue[key] === 'number' && isNaN(newVal) && isNaN(oldValue[key]);

                                if (newVal !== oldValue[key] && !bothNaN) {
                                    changeCount++;
                                    oldValue[key] = newVal;
                                }
                            } else {
                                //La propriété est présente dans le nouvel objet et pas dans l'ancien, il s'agit forcément d'un ajout
                                changeCount++;
                                oldLength++;
                                oldValue[key] = newVal;
                            }
                        });

                        //il y + de propriété dans l'ancien objet que dans le nouveau donc il y a eu des suppressions
                        if (oldLength > newLength) {
                            changeCount++;
                            _.forOwn(oldValue, function (oldVal, key) {
                                if (!newValue.hasOwnProperty(key)) {
                                    oldLength--;
                                    delete oldValue[key];
                                }
                            });
                        }
                    }
                } else {
                    if (!self.$$areEqual(newValue, oldValue, false)) {
                        changeCount++;
                    }

                    oldValue = newValue;
                }

                return changeCount;
            };

            var internalListenerFn = function () {
                listenerFn(newValue, firstRun ? newValue : veryOldValue, self);

                if (trackVeryOldValue) {
                    veryOldValue = _.clone(newValue);
                }

                firstRun = false;

            };

            return this.$watch(internalWatchFn, internalListenerFn);
        };

        Scope.prototype.$on = function (eventName, listener) {
            var listeners = this.$$listeners[eventName];
            if (!listeners) {
                this.$$listeners[eventName] = listeners = [];
            }
            listeners.push(listener);

            return function () {
                var index = listeners.indexOf(listener);

                if (index >= 0) {
                    listeners[index] = null;
                }

            };
        };

        Scope.prototype.$$fireEventOnScope = function (eventName, listenerArgs) {
            var listeners = this.$$listeners[eventName] || [];

            var i = 0;
            while (i < listeners.length) {
                if (listeners[i] === null) {
                    listeners.splice(i, 1);
                } else {
                    try{
                        listeners[i].apply(null, listenerArgs);
                    }catch(e){
                        console.error(e);
                    }finally{
                        i++;
                    }
                }
            }
        };

        /**
         * Emission d'un event
         * @param eventName
         * @returns {{name: *}}
         */
        Scope.prototype.$emit = function (eventName) {

            var propagationStopped = false;
            //targetScope est le scope émetteur de l'évènement
            var event = {
                name: eventName,
                targetScope: this,
                stopPropagation: function () {
                    propagationStopped = true;
                },
                preventDefault: function () {
                    event.defaultPrevented = true;
                }
            };

            var listenerArgs = [event].concat(_.rest(arguments));
            var scope = this;

            //l'émission remonte les parents (bottom - up)
            do {
                event.currentScope = scope;
                scope.$$fireEventOnScope(eventName, listenerArgs);
                scope = scope.$parent;
            } while (scope && !propagationStopped);

            event.currentScope = null;

            return event;
        };

        /**
         * broadcast d'un event
         * @param eventName
         * @returns {{name: *}}
         */
        Scope.prototype.$broadcast = function (eventName) {
            //targetScope est le scope émetteur de l'évènement
            var event = {
                name: eventName,
                targetScope: this,
                preventDefault: function () {
                    event.defaultPrevented = true;
                }
            };
            var listenerArgs = [event].concat(_.rest(arguments));

            //Le broadcast coûte très cher si il est lancé depuis le root scope !
            //top down
            this.$$everyScope(function (scope) {
                event.currentScope = scope;
                scope.$$fireEventOnScope(eventName, listenerArgs);
                return true;
            });

            event.currentScope = null;

            return event;
        };

        var $rootScope = new Scope();

        return $rootScope;
    }];
}

