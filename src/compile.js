/*jshint globalstrict: true*/
'use strict';

var PREFIX_REGEXP = /(x[\:\-_]|data[\:\-_])/i;

/**
 *{
 *    anAttr: '@'
 *}
 *
 *And returns the following:
 *{
 *    anAttr: {
 *       mode: '@'
 *    }
 *}
 */
function parseIsoltateBindings(scope) {
    var bindings = {};

    _.forEach(scope, function(definition, scopeName) {
        var match = definition.match(/\s*([@&]|=(\*?))\s*(\w*)\s*/);
        bindings[scopeName] = {
            mode: match[1][0],
            collection: match[2] === '*',
            attrName: match[3] || scopeName
        };
    });
    return bindings;
}


function $CompileProvider($provide) {

    function directiveNormalize(name) {
        //suppression des prefix et normalisation du nom
        return _.camelCase(name.replace(PREFIX_REGEXP, ''));
    }

    /**
     * compare 2 directives
     * @param a
     * @param b
     */
    function byPriority(a, b) {

        var diff = b.priority - a.priority;

        if (diff !== 0) {
            return diff;
        } else {
            if (a.name !== b.name) {
                return (a.name < b.name ? -1 : 1);
            } else {
                return a.index - b.index;
            }

        }

    }

    var hasDirectives = {};

    this.directive = function(name, directiveFactory) {

        if (_.isString(name)) {
            if (name === 'hasOwnProperty') {
                throw 'hasOwnProperty is not a valid directive name';
            }

            if (!hasDirectives.hasOwnProperty(name)) {
                hasDirectives[name] = [];
                $provide.factory(name + 'Directive', ['$injector', function($injector) {
                    var factories = hasDirectives[name];
                    return _.map(factories, function(factory, i) {
                        var directive = $injector.invoke(factory);
                        directive.restrict = directive.restrict || 'EA';
                        directive.name = directive.name || name;
                        directive.index = i; //ordre d'enregistrement de la directive
                        //si la directive a un controller mais pas de require on auto passe le controller de la directive
                        directive.require = directive.require || (directive.controller && name);
                        directive.prioriy = directive.priority || 0;
                        if (directive.link && !directive.compile) {
                            directive.compile = _.constant(directive.link);
                        }
                        //isolate scope
                        if (_.isObject(directive.scope)) {
                            directive.$$isolateBindings = parseIsoltateBindings(directive.scope);
                        }

                        return directive;
                    });
                }]);
            }
            hasDirectives[name].push(directiveFactory);
        } else {
            _.forEach(name, function(directiveFactory, name) {
                this.directive(name, directiveFactory);
            }, this);
        }
    };

    this.$get = ['$injector', '$controller', '$rootScope', '$parse', '$http',
        function($injector, $controller, $rootScope, $parse, $http) {

            function Attributes(element) {
                this.$$element = element;
                this.$attr = {}; //objet pour conserver le mapping nom normalisé/ nom dénormalisé des attributs
            }

            Attributes.prototype.$addClass = function(classVal) {
                this.$$element.addClass(classVal);
            };

            Attributes.prototype.$removeClass = function(classVal) {
                this.$$element.removeClass(classVal);
            };

            Attributes.prototype.$updateClass = function(newClassVal, oldClassVal) {
                var newClasses = newClassVal.split(/\s+/);
                var oldClasses = oldClassVal.split(/\s+/);

                var addedClasses = _.difference(newClasses, oldClasses);
                var removedClasses = _.difference(oldClasses, newClasses);

                if (addedClasses.length) {
                    this.$addClass(addedClasses.join(' '));
                }

                this.$$element.removeClass(removedClasses);
            };

            Attributes.prototype.$set = function(key, value, writeAttr, attrName) {
                this[key] = value;

                if (isBooleanAttribute(this.$$element[0], key)) {
                    this.$$element.prop(key, value);
                }

                if (!attrName) {

                    if (this.$attr[key]) {
                        attrName = this.$attr[key];
                    } else {
                        attrName = this.$attr[key] = _.kebabCase(key, '-');
                    }
                } else {
                    this.$attr[key] = attrName;
                }

                if (writeAttr !== false) {
                    this.$$element.attr(attrName, value);
                }

                //déclenchement des observers à la fin du $set

                if (this.$$observers) {
                    _.forEach(this.$$observers[key], function(observer) {

                        try {
                            observer(value);
                        } catch (e) {
                            console.log(e);
                        }
                    });
                }

            };

            /**
             * Observe function. Ajoute au tableau des observers de la clé key le callback fn
             * $$observers est un objet
             * @param key
             * @param fn
             */
            Attributes.prototype.$observe = function(key, fn) {
                var self = this;
                this.$$observers = this.$$observers || Object.create(null);
                this.$$observers[key] = this.$$observers[key] || [];
                this.$$observers[key].push(fn);
                $rootScope.$evalAsync(function() {
                    fn(self[key]);
                });

                return function() {
                    var index = self.$$observers[key].indexOf(fn);

                    if (index >= 0) {
                        self.$$observers[key].splice(index, 1);
                    }
                };
            };

            var BOOLEAN_ATTRS = {
                multiple: true,
                selected: true,
                checked: true,
                disabled: true,
                readOnly: true,
                required: true,
                open: true
            };

            var BOOLEAN_ELEMENTS = {
                INPUT: true,
                SELECT: true,
                OPTION: true,
                TEXTAREA: true,
                BUTTON: true,
                FORM: true,
                DETAILS: true
            };

            function isBooleanAttribute(node, attrName) {
                return BOOLEAN_ATTRS[attrName] && BOOLEAN_ELEMENTS[node.nodeName];
            }

            function compile($compileNodes) {
                var compositeLinkFn = compileNodes($compileNodes);

                //renvoi la fonction de link
                return function publicLinkFn(scope) {
                    $compileNodes.data('$scope', scope); //attache le scope au noeud compilé
                    compositeLinkFn(scope, $compileNodes);
                    return $compileNodes;
                };
            }

            function compileNodes($compileNodes) {
                //récupéreation des directives pour tous les noeuds DOM auxquels elles vont s'appliquer
                var linkFns = []; //fonction stockant pour chaque noeud d'un niveau n la compositeLinkFn
                _.forEach($compileNodes, function(node, i) {
                    var attrs = new Attributes($(node));
                    var directives = collectDirectives(node, attrs);
                    var nodeLinkFn;

                    if (directives.length) {
                        //nodeLinkFn est la fonction qui lorsqu'elle est exécutée,
                        //exécute tous les link de toutes les directives applicable au noeud
                        //en cours d'itération
                        nodeLinkFn = applyDirectivesToNode(directives, node, attrs); //applique les directives aux noeud
                    }

                    var childLinkFn;

                    if ((!nodeLinkFn || !nodeLinkFn.terminal) &&
                        node.childNodes && node.childNodes.length) {
                        //pour les noeuds dom imbriqués
                        //récupère la composite link function qui exécutera pour chaque child node
                        //toutes les links fn.
                        childLinkFn = compileNodes(node.childNodes);
                    }

                    if (nodeLinkFn && nodeLinkFn.scope) {
                        attrs.$$element.addClass('ng-scope');
                    }

                    if (nodeLinkFn || childLinkFn) {
                        linkFns.push({
                            nodeLinkFn: nodeLinkFn,
                            childLinkFn: childLinkFn,
                            idx: i
                        });
                    }
                });

                function compositeLinkFn(scope, linkNodes) {
                    //Maintien une liste des noeuds à leur position original
                    //en cas de link function altérant le dom
                    var stableNodeList = [];

                    _.forEach(linkFns, function(linkFn) {
                        var nodeIdx = linkFn.idx;
                        stableNodeList[nodeIdx] = linkNodes[nodeIdx];
                    });

                    _.forEach(linkFns, function(linkFn) {
                        //récupération du noeud dom concerné par le link
                        var node = stableNodeList[linkFn.idx];

                        if (linkFn.nodeLinkFn) {
                            var childScope;

                            if (linkFn.nodeLinkFn.scope) {
                                childScope = scope.$new();
                                //au moins une directive du noeud a requis un new scope
                                $(node).data('$scope', childScope);
                            }else{
                                childScope = scope;
                            }

                            var boundTranscludeFn;

                            if(linkFn.nodeLinkFn.transcludeOnThisElement){
                                boundTranscludeFn = function(transcludedScope, containingScope){

                                    if(!transcludedScope){
                                        transcludedScope = scope.$new(false, containingScope);
                                    }

                                    return linkFn.nodeLinkFn.transclude(transcludedScope);
                                }
                            }

                            linkFn.nodeLinkFn(
                                linkFn.childLinkFn,
                                childScope,
                                node,
                                boundTranscludeFn
                            );
                        } else {
                            linkFn.childLinkFn(
                                scope,
                                node.childNodes
                            );
                        }
                    });
                }
                return compositeLinkFn;
            }

            function directiveIsMultiElement(name) {
                if (hasDirectives.hasOwnProperty(name)) {
                    var directives = $injector.get(name + 'Directive');
                    return _.any(directives, {
                        multiElement: true
                    });
                }
                return false;
            }

            function collectDirectives(node, attrs) {
                var directives = [];
                var match;

                if (node.nodeType === Node.ELEMENT_NODE) {
                    var normalizedNodeName = directiveNormalize(nodeName(node).toLowerCase());
                    addDirective(directives, normalizedNodeName, 'E');

                    _.forEach(node.attributes, function(attr) {
                        var attrStartName, attrEndName;
                        var name = attr.name;
                        var normalizedAttr = directiveNormalize(name.toLowerCase());
                        var isNgAttr = /^ngAttr[A-Z]/.test(normalizedAttr);
                        if (isNgAttr) {
                            name = _.kebabCase(
                                normalizedAttr[6].toLowerCase() +
                                normalizedAttr.substring(7)
                            );
                            normalizedAttr = directiveNormalize(name.toLowerCase());
                        }
                        attrs.$attr[normalizedAttr] = name;

                        var directiveNName = normalizedAttr.replace(/(Start|End)$/, '');
                        if (directiveIsMultiElement(directiveNName)) {
                            if (/Start$/.test(normalizedAttr)) {
                                attrStartName = name;
                                attrEndName = name.substring(0, name.length - 5) + 'end';
                                name = name.substring(0, name.length - 6);
                            }
                        }
                        normalizedAttr = directiveNormalize(name.toLowerCase());
                        addDirective(directives, normalizedAttr, 'A', attrStartName, attrEndName);

                        if (isNgAttr || !attrs.hasOwnProperty(normalizedAttr)) {
                            attrs[normalizedAttr] = attr.value.trim();
                            if (isBooleanAttribute(node, normalizedAttr)) {
                                attrs[normalizedAttr] = true;
                            }
                        }
                    });

                    var className = node.className;

                    if (_.isString(className) && !_.isEmpty(className)) {

                        while ((match = /([\d\w\-_]+)(?:\:([^;]+))?;?/.exec(className))) {
                            var normalizedClassName = directiveNormalize(match[1]);
                            if (addDirective(directives, normalizedClassName, 'C')) {
                                attrs[normalizedClassName] = match[2] ? match[2].trim() : undefined;
                            }
                            className = className.substr(match.index + match[0].length);
                        }
                    }
                } else if (node.nodeType === Node.COMMENT_NODE) {
                    match = /^\s*directive\:\s*([\d\w\-_]+)\s*(.*)$/.exec(node.nodeValue);
                    if (match) {
                        var normalizedName = directiveNormalize(match[1]);

                        if (addDirective(directives, directiveNormalize(match[1]), 'M')) {
                            attrs[normalizedName] = match[2] ? match[2].trim() : undefined;
                        }
                    }
                }

                directives.sort(byPriority);

                return directives;
            }

            function nodeName(element) {
                return element.nodeName ? element.nodeName : element[0].nodeName;
            }

            function addDirective(directives, name, mode, attrStartName, attrEndName) {
                var match;
                if (hasDirectives.hasOwnProperty(name)) {
                    var foundDirectives = $injector.get(name + 'Directive');
                    //filtrage des directive en fonction du restrict
                    var applicableDirectives = _.filter(foundDirectives, function(dir) {
                        return dir.restrict.indexOf(mode) !== -1;
                    });

                    _.forEach(applicableDirectives, function(directive) {
                        if (attrStartName) {
                            directive = _.create(directive, {
                                $$start: attrStartName,
                                $$end: attrEndName
                            });
                        }

                        directives.push(directive);
                        match = directive;
                    });
                }

                return match;
            }

            function groupScan(node, startAttr, endAttr) {
                var nodes = [];
                if (startAttr && node && node.hasAttribute(startAttr)) {
                    var depth = 0;
                    do {
                        if (node.nodeType === Node.ELEMENT_NODE) {
                            if (node.hasAttribute(startAttr)) {
                                depth++;
                            } else if (node.hasAttribute(endAttr)) {
                                depth--;
                            }
                        }
                        nodes.push(node);
                        node = node.nextSibling;
                    } while (depth > 0);
                } else {
                    nodes.push(node);
                }
                return $(nodes);
            }

            function groupElementsLinkFnWrapper(linkFn, attrStart, attrEnd) {
                return function(scope, element, attrs, ctrl) {
                    var group = groupScan(element[0], attrStart, attrEnd);
                    return linkFn(scope, group, attrs, ctrl);
                };
            }

            function compileTemplateUrl(directives, $compileNode, attrs, previousCompileContext) {
                var origAsyncDirective = directives.shift();
                var derivedSyncDirective = _.extend({},
                    origAsyncDirective, {
                        templateUrl: null
                    }
                );

                var templateUrl = _.isFunction(origAsyncDirective.templateUrl) ?
                    origAsyncDirective.templateUrl($compileNode, attrs) :
                    origAsyncDirective.templateUrl;
                var afterTemplateNodeLinkFn, afterTemplateChildLinkFn;
                var linkQueue = [];
                //clear the node
                $compileNode.empty();
                $http.get(templateUrl).success(function(template) {
                    directives.unshift(derivedSyncDirective);
                    $compileNode.html(template);
                    //poursuite de l'application des directives au noeud
                    afterTemplateNodeLinkFn = applyDirectivesToNode(directives, $compileNode, attrs, previousCompileContext);
                    afterTemplateChildLinkFn = compileNodes($compileNode[0].childNodes);

                    _.forEach(linkQueue, function(linkCall) {
                        afterTemplateNodeLinkFn(
                            afterTemplateChildLinkFn, linkCall.scope, linkCall.linkNode);
                    });

                    //le template a été reçu
                    linkQueue = null;
                });

                return function delayedNodeLinkFn(_ignoreChildLinkFn, scope, linkNode) {

                    if (linkQueue) {
                        //on attend que des templates soient chargés
                        linkQueue.push({
                            scope: scope,
                            linkNode: linkNode
                        });
                    } else {
                        afterTemplateNodeLinkFn(afterTemplateChildLinkFn, scope, linkNode);
                    }
                };

            }

            function applyDirectivesToNode(directives, compileNode, attrs, previousCompileContext) {
                previousCompileContext = previousCompileContext || {};
                var $compileNode = $(compileNode);
                var terminalPriority = -Number.MAX_VALUE;
                var terminal = false;
                var preLinkFns = [] = previousCompileContext.preLinkFns || [];
                var postLinkFns = [] = previousCompileContext.postLinkFns || [];
                var controllers = {};
                var newScopeDirective;
                var newIsolateScopeDirective = previousCompileContext.newIsolateScopeDirective;
                var templateDirective = previousCompileContext.templateDirective;
                var controllerDirectives = previousCompileContext.controllerDirectives;
                var childTranscludeFn, hasTranscludeDirective;

                function getControllers(require, $element) {

                    //recherche du matching chapeau ^

                    if (_.isArray(require)) {
                        return _.map(require, getControllers);
                    } else {
                        var value;
                        var match = require.match(/^(\^\^?)?(\?)?(\^\^?)?/);
                        var optional = match[2]; //le controller requis est -il optionnel ?

                        require = require.substring(match[0].length);

                        if (match[1] || match[3]) {
                            if (match[3] && !match[1]) {
                                match[1] = match[3];
                            }
                            //le chapeau a été trouvé
                            //est-ce un double chapeau ?
                            if (match[1] === '^^') {
                                $element = $element.parent(); //on prend direct le parent et on ne se soucie pas noeuds frères
                            }
                            //On remonte le dom pour trouver un node qui contiendrait le controller requis (i.e. via le nom de la directive)
                            while ($element.length) {
                                value = $element.data('$' + require + 'Controller');

                                if (value) {
                                    //on a trouvé
                                    break;
                                } else {
                                    $element = $element.parent();
                                }
                            }
                        } else {
                            if (controllers[require]) {
                                value = controllers[require].instance;
                            }
                        }

                        if (!value && !optional) {
                            throw 'Controller ' + require + ' required by directive, cannot be found!';
                        }
                        return value || null;
                    }
                }

                function addLinkFns(preLinkFn, postLinkFn, attrStart, attrEnd, isolateScope, require) {
                    if (preLinkFn) {
                        if (attrStart) {
                            preLinkFn = groupElementsLinkFnWrapper(preLinkFn, attrStart, attrEnd);
                        }
                        preLinkFn.isolateScope = isolateScope;
                        preLinkFn.require = require;
                        preLinkFns.push(preLinkFn);
                    }
                    if (postLinkFn) {
                        if (attrStart) {
                            postLinkFn = groupElementsLinkFnWrapper(postLinkFn, attrStart, attrEnd);
                        }
                        postLinkFn.isolateScope = isolateScope;
                        postLinkFn.require = require;
                        postLinkFns.push(postLinkFn);
                    }
                }

                _.forEach(directives, function(directive, i) {

                    if (directive.$$start) {
                        $compileNode = groupScan(compileNode, directive.$$start, directive.$$end);
                    }

                    if (directive.priority < terminalPriority) {
                        return false;
                    }

                    if (directive.scope) {

                        if (_.isObject(directive.scope)) {

                            if (newIsolateScopeDirective || newScopeDirective) {
                                throw 'Multiple directives asking for new/inherited scope';
                            }
                            newIsolateScopeDirective = directive;
                        } else {
                            if (newIsolateScopeDirective) {
                                throw 'Multiple directives asking for new/inherited scope';
                            }
                            newScopeDirective = newScopeDirective || directive;
                        }
                    }

                    if (directive.terminal) {
                        terminal = true;
                        terminalPriority = directive.priority;
                    }

                    if (directive.controller) {
                        //on stocke pour chaque nom de directive le controller associé
                        controllerDirectives = controllerDirectives || {};
                        controllerDirectives[directive.name] = directive;
                    }

                    if(directive.transclude){

                        if(hasTranscludeDirective){
                            throw 'Multiple directives asking for transclude';
                        }
                        hasTranscludeDirective = true;
                        var $transcludedNodes = $compileNode.clone().contents();
                        //stocke la public link fn
                        childTranscludeFn = compile($transcludedNodes);
                        $compileNode.empty();
                    }

                    if (directive.template) {
                        if (templateDirective) {
                            throw 'Multiple directives asking for template';
                        }
                        templateDirective = directive;
                        $compileNode.html(_.isFunction(directive.template) ?
                            directive.template($compileNode, attrs) :
                            directive.template);
                    }

                    //si une template url est spécifié il faut attendre le chargement
                    //du template avant de compiler la directive et les autres directives du noeud.
                    if (directive.templateUrl) {

                        if (templateDirective) {
                            throw 'Multiple directives asking for template';
                        }

                        templateDirective = directive;

                        nodeLinkFn = compileTemplateUrl(_.drop(directives, i),
                         $compileNode,
                         attrs, {
                            templateDirective: templateDirective,
                            newIsolateScopeDirective: newIsolateScopeDirective,
                            controllerDirectives: controllerDirectives,
                            preLinkFns : preLinkFns,
                            postLinkFns : postLinkFns
                        });
                        return false;
                    } else if (directive.compile) {
                        var linkFn = directive.compile($compileNode, attrs);
                        //la directive est-elle isolée ?
                        var isolateScope = (directive === newIsolateScopeDirective);
                        var attrStart = directive.$$start;
                        var attrEnd = directive.$$end;
                        var require = directive.require; //la directive utilise -t-elle le controller d'une autre directive ?

                        if (_.isFunction(linkFn)) {
                            addLinkFns(null, linkFn, attrStart, attrEnd, isolateScope, require);
                        } else if (linkFn) {
                            addLinkFns(linkFn.pre, linkFn.post, attrStart, attrEnd, isolateScope, require);
                        }
                    }
                });

                /*
                 * Fonction exécutant pour un noeaud linkNode l'ensemble des post et pre
                 * link function définie par les directives appliqués au noeud.
                 * Déclenche également l'xxécution des toutes les link function sur le niveau
                 * n-1 de l'arbre dom.
                 */
                function nodeLinkFn(childLinkFn, scope, linkNode, boundTranscludeFn) {

                    var $element = $(linkNode);

                    //instanciation de l'isolate scope avant création du controllers
                    //des fois qu'il faille le passer au contrôleur
                    //la directive a-t-elle requis un scope  isolé ?
                    var isolateScope;

                    if (newIsolateScopeDirective) {
                        isolateScope = scope.$new(true);
                        $element.addClass('ng-isolate-scope');
                        $element.data('$isolateScope', isolateScope);
                    }

                    //y a t il des controller à instancier ?
                    if (controllerDirectives) {
                        //instanciation de tous les controllers via le controllerProvider
                        _.forEach(controllerDirectives, function(directive) {
                            var locals = {
                                //besoin d'avoir l'isolate scope si il y en a un
                                $scope: directive === newIsolateScopeDirective ? isolateScope : scope,
                                $element: $element,
                                $attrs: attrs
                            };

                            var controllerName = directive.controller;
                            if (controllerName === '@') {
                                //my-directive="MyController"
                                controllerName = attrs[directive.name];
                            }
                            //rattachement du controller au scope si le controllerAs a été précisé
                            var controller = $controller(controllerName, locals, true, directive.controllerAs);
                            controllers[directive.name] = controller;
                            //ajout du controller dans le dom au niveau de l'élement
                            $element.data('$' + directive.name + 'Controller', controller.instance);
                        });
                    }


                    //la directive a-t-elle requis un scope  isolé ?
                    var isolateScope;

                    if (newIsolateScopeDirective) {

                        var isolateContext = isolateScope;
                        //besoin d'avoir le controller si le bindToController est spécifié
                        //la directive a t elle requis un bind to controller ?
                        if (newIsolateScopeDirective.bindToController) {
                            //instance du semi constructed controller
                            isolateContext = controllers[newIsolateScopeDirective.name].instance;
                        }

                        _.forEach(newIsolateScopeDirective.$$isolateBindings,
                            function(definition, scopeName) {
                                var attrName = definition.attrName;
                                switch (definition.mode) {
                                    //binding d'attribut à pousser dans le scope
                                    case '@':
                                        attrs.$observe(attrName, function(newAttrValue) {
                                            //c'est le observe sur l'attribute qui met à jour le scope
                                            isolateContext[scopeName] = newAttrValue;
                                        });

                                        //si l'attribut existe déjà dans les attrs on le pousse direct dans le scope
                                        if (attrs[attrName]) {
                                            isolateContext[scopeName] = attrs[attrName];
                                        }

                                        break;
                                    case '=':
                                        //parse the attribute expression
                                        var parentGet = $parse(attrs[attrName]); // le parentGet sera évalué en fonction du scope parent
                                        var lastValue = isolateContext[scopeName] = parentGet(scope); //affectation de la valeur dans l'isolate scope

                                        //ajout d'un watcher pour détecter les changements dans le scope parent
                                        //et mettre à jour la valeur dans l'isolate scope
                                        var parentValueWatch = function() {
                                            var parentValue = parentGet(scope);
                                            if (isolateContext[scopeName] !== parentValue) {
                                                if (parentValue !== lastValue) {
                                                    //c'est la valeur du parent scope qui a été modifié
                                                    isolateContext[scopeName] = parentValue;
                                                } else {
                                                    //c'est la valeur dans l'isolate scope qui a été modifié
                                                    //MAJ du parent scope
                                                    parentValue = isolateScope[scopeName];
                                                    //affectation dans le scope du parent
                                                    parentGet.assign(scope, parentValue);
                                                }
                                            }
                                            //la value contient la dernière valeur à jour
                                            lastValue = parentValue;
                                            return lastValue;
                                        };
                                        var unwatch;

                                        if (definition.collection) {
                                            unwatch = scope.$watchCollection(attrs[attrName], parentValueWatch);
                                        } else {
                                            unwatch = scope.$watch(parentValueWatch);
                                        }
                                        isolateScope.$on('$destroy', unwatch);
                                        break;

                                    case '&':
                                        var parentExpr = $parse(attrs[attrName]);
                                        isolateContext[scopeName] = function(locals) {
                                            return parentExpr(scope, locals);
                                        };
                                        break;
                                }

                            });
                    }

                    _.forEach(controllers, function(controller) {
                        //instanciation final des semi-constructed controller jusqu'alors.
                        controller();
                    });

                    function boundTranscludeFn(){
                        return childTranscludeFn(scope);
                    }

                    function scopeBoundTranscludeFn(transcludedScope){
                        return boundTranscludeFn(transcludedScope,scope);
                    }

                    _.forEach(preLinkFns, function(linkFn) {
                        linkFn(linkFn.isolateScope ? isolateScope : scope,
                            $element,
                            attrs,
                            linkFn.require && getControllers(linkFn.require, $element), //un controller d'une autre directive doit-il être passer à la linkFn ?
                            scopeBoundTranscludeFn
                        );
                    });
                    if (childLinkFn) {

                        var scopeToChild = scope;

                        if (newIsolateScopeDirective && newIsolateScopeDirective.template) {
                            //si la directive possède un template et un scope isolé
                            scopeToChild = isolateScope;
                        }

                        childLinkFn(scopeToChild, linkNode.childNodes);
                    }
                    _.forEachRight(postLinkFns, function(linkFn) {
                        linkFn(linkFn.isolateScope ? isolateScope : scope,
                            $element,
                            attrs,
                            linkFn.require && getControllers(linkFn.require, $element),
                            scopeBoundTranscludeFn
                        );
                    });
                }

                nodeLinkFn.terminal = terminal;
                //indique si pour le pool de nodeLinkFn à appliquer au node il faut créer un nouveau
                //scope ou utiliser celui de l'environnement.
                nodeLinkFn.scope = newScopeDirective && newScopeDirective.scope;
                nodeLinkFn.transcludeOnThisElement = hasTranscludeDirective;
                nodeLinkFn.transclude = childTranscludeFn;

                return nodeLinkFn;
            }

            return compile;
        }
    ];

}
