/* jshint globalstrict: true */
/* global angular: false, HashMap: false */

var FN_ARGS = /^function\s*[^\(]*\(\s*([^\)]*)\)/m;

//regex pour supprimer les espace entre le arguments
var FN_ARG = /^\s*(_?)(\S+?)\1\s*$/;

//regex pour trouver les lignes de commentaires
//ajout du /g à la fin pour matcher plusieurs fois
var STRIP_COMMENTS = /(\/\/.*$)|(\/\*.*?\*\/)/mg;

var INSTANTIATING = { };




function createInjector(modulesToLoad, strictDi){

    function enforceReturnValue(factoryFn){

        return function(){
            var value = instanceInjector.invoke(factoryFn);

            if(_.isUndefined(value)){
                throw 'factory must return a value';
            }
            return value;
        }
    }

    var providerCache = {};
    var providerInjector = providerCache.$injector = createInternalInjector(providerCache, function(){
        //si l'on passe dans la factory function cela veut dire que le provider recherché
        //n'a pas été trouvé dans le providerCache, donc qu'il n'a pas été enregistré à la création de l'injector
        throw 'Unknown provider: '+path.join(' <- ');
    });

    var instanceCache = {};

    //instanceCache.$injector  ajoute l'injector d'instance dans l'instanceCache
    //pour pouvoir être injecté par DI
    var instanceInjector = instanceCache.$injector =  createInternalInjector(instanceCache, function(name){
        var provider = providerInjector.get(name + 'Provider');
        return instanceInjector.invoke(provider.$get,provider);
    });


    var loadedModules = new HashMap();
    var path = [];      //path de résolution des dependances
    var strictDi = (strictDi === true);

    // objet permettant de fournir des features (provider, constant) dans les cache
     providerCache.$provide =  {
        constant : function(key, value){
            if(key === 'hasOwnProperty'){
                throw 'hasOwnProperty is not a valid constant name !';
            }
            //la constante est positionnée dans le cache.
            //On autorise pas le hasOwnProperty a être ajouté dans le cache pour ne pas
            //masquer les hasOwnProperty utilisé dans le has
            providerCache[key] = value;
            instanceCache[key] = value;
        },
        //le provider insère les données qu'il produit dans l'instanceCache
        provider : function(key, provider){

            //le provider est une fonction constructor
            if(_.isFunction(provider)){
               provider =  providerInjector.instantiate(provider);
                //via instantiate la fonction est exécutée.
            }

            //on stocke l'objet provider dans le cache des providers pour l'utiliser
            //de manière lazily
            providerCache[key + 'Provider'] = provider;
        },
        factory : function(key, factoryFn, enforce){
            //enforce à false pour laisser la valeur undefined possible pour les values
            this.provider(key, { $get: enforce === false ? factoryFn : enforceReturnValue(factoryFn)});
        },
        value : function(key, value){
            this.factory(key, _.constant(value), false);
        },
        service : function(key, Constructor){
            this.factory(key, function(){
                return instanceInjector.instantiate(Constructor);
            });

        },
        decorator : function(serviceName, decoratorFn){
            //récupération du provider enregdistré pour obtenir l'objet à décorer depuis le providercache
            var provider = providerInjector.get(serviceName + 'Provider');

            var original$get = provider.$get;

            //override du $get
            provider.$get = function(){
                //récupération du $get. Injection de dépendance activé
                var instance = instanceInjector.invoke(original$get, provider);

                //appel de la fonction décoration, en ajoutant en locals l'objet contenant $decorate qui est
                //une référence à l'objet original. Permet d'injecter en dépendance l'instance original
                instanceInjector.invoke(decoratorFn, null, {$delegate : instance});
                return instance;
            }


        }
    };

    /**
     * Inernal injector
     * @param cache
     * @param factoryFn
     * @returns {{has: Function, get: getService, invoke: invoke, annotate: annotate, instantiate: instantiate}}
     */
    function createInternalInjector(cache, factoryFn){

        /**
         * Renvoie la valeur en cache de la clé name.
         * Si name est une clé de provider, le provider est exécuté et renvoie son résultat
         * @param name
         * @returns {*}
         */
        function getService(name){

            if(cache.hasOwnProperty(name)){
                if(cache[name] === INSTANTIATING){
                    throw new Error('Circular dependency found: ' + name + ' <- ' + path.join(' <- '));
                }
                return cache[name];
            } else {
                path.unshift(name);
                cache[name] = INSTANTIATING;
                try{
                    //invoke depuis le cache des provider
                    return (cache[name] = factoryFn(name));
                } finally{
                    path.shift();
                    if(cache[name] === INSTANTIATING){
                        delete cache[name];
                    }
                }

            }
        }

        /**
         * Instantiate
         * @param Type
         * @param locals
         * @returns {UnwrappedType}
         */
        function instantiate(Type, locals){

            var UnwrappedType = _.isArray(Type) ? _.last(Type) : Type;

            var instance = Object.create(UnwrappedType.prototype); // utilisation de la fonction create pour conserver le prototype

            invoke(Type, instance, locals);

            return instance;
        }

        /**
         * Invokation par l'injector de la fonction fn. Passe par la résolution
         * de dépendance via le modules enregistrés
         * @param fn
         * @param self
         * @param locals
         * @returns {*}
         */
        function invoke(fn, self, locals){

            //résolution pour l'injection de dépendances à travers les valeurs
            //enregistrées auprès de l'injector
            //annotate extrait les arguments de fn qu'il soit dans le $inject ou un array
            var args = _.map(annotate(fn), function(token){
                if(_.isString(token)){
                    //les locals ovveride toujour les champs enregistrés via les modules
                    return locals && locals.hasOwnProperty(token) ? locals[token]: getService(token);
                }else {
                    throw 'Incorrect injection token! Expected a string, got '+token;
                }
            });

            if(_.isArray(fn)){
                fn = _.last(fn);
            }

            return fn.apply(self, args);
        }


        return {
            has : function(key){
                return cache.hasOwnProperty(key)
                    || providerCache.hasOwnProperty(key + 'Provider');
            },

            get : getService,

            invoke : invoke,

            annotate : annotate,

            instantiate : instantiate
        };
    }


    function annotate(fn){

        if(_.isArray(fn)){
            return fn.slice(0, fn.length - 1);
        }else if(fn.$inject){
            return fn.$inject;
        }else if(!fn.length) {
            return [];
        }else{

            if(strictDi){
                throw 'fn is not using explicit annotation and '+
                'cannot be invoked in strict mode';
            }

            //suppression des commentaires dans les param de fonction
            var source = fn.toString().replace(STRIP_COMMENTS,'');

            var argDeclaration = source.match(FN_ARGS);

            return _.map(argDeclaration[1].split(','), function(argName){
                return argName.match(FN_ARG)[2];
            });
        }

    }

    /**
     *
     * @param queue
     */
    function runInvokeQueue(queue) {
        _.forEach(queue, function(invokeArgs) {
            //le providerInjector possède dans son cache le $provide
            //et le $injector
            var service = providerInjector.get(invokeArgs[0]);
            var method = invokeArgs[1];
            var args = invokeArgs[2];
            service[method].apply(service, args);
        });
    }

    var runBlocks = [];

    _.forEach(modulesToLoad, function loadModule(module){

        if(!loadedModules.get(module)) {
            loadedModules.put(module, true);
            if (_.isString(module)) {
                //check que le module n'a pas déjà été chargé

                var module = angular.module(module);
                //chargement des module requis en récursif
                _.forEach(module.requires, loadModule);
                runInvokeQueue(module._invokeQueue);
                runInvokeQueue(module._configBlocks);
                runBlocks = runBlocks.concat(module._runBlocks);

            } else if (_.isFunction(module) || _.isArray(module)) {
                //execution d'une function module. création d'un module en dépendance via une fonction
                runBlocks.push(providerInjector.invoke(module));
            }
        }
    });

    //_.compact supprimer les valeur falsy de la collection
    _.forEach(_.compact(runBlocks) , function(runBlock){
        instanceInjector.invoke(runBlock);
    });

    return instanceInjector;
}
