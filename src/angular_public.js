
function publishExternalAPI(){
    'use strict';

    setupModuleLoader(window);

    //création du module ng
    //The ng module is where all the services, directives, and other components provided by
    //Angular itself will be
    var ngModule = angular.module('ng', []);

    ngModule.provider('$parse', $ParseProvider);

    ngModule.provider('$rootScope', $RootScopeProvider);

    ngModule.provider('$q', $QProvider);

    ngModule.provider('$$q', $$QProvider);

    ngModule.provider('$httpBackend', $HttpBackendProvider);

    ngModule.provider('$http', $HttpProvider);

    ngModule.provider('$compile', $CompileProvider);

    ngModule.provider('$controller', $ControllerProvider);

    ngModule.directive('ngController', ngControllerDirective);
}
