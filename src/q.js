/* jshint globalstrict: true */
'use strict';


function qFactory(callLater){

    function makePromise(value, resolved){
        var d = new Deferred();

        if(resolved){
            d.resolve(value);
        }else{
            d.reject(value);
        }

        return d.promise;
    }

    function scheduleProcessQueue(state){
        callLater(function(){
            processQueue(state);
        });
    }

    function processQueue(state){
        var pending = state.pending;
        //On supprime les callback qui vont être joué.
        //placer en amont au cas ou des callback ajouterai des callback
        delete state.pending;

        _.forEach(pending, function(handlers){
            //On exécute le handlers positionné en 1 (success) ou en 2(reject)
            var deferred = handlers[0];
            var fn = handlers[state.status];
            try {
                if (_.isFunction(fn)) {
                    deferred.resolve(fn(state.value));
                } else if (state.status === 1) {
                    //c'est un succès et on a pas de callback de succès sur cette promise on délègue au defer
                    deferred.resolve(state.value);
                } else if (state.status === 2) {
                    //c'est un échec et on a pas de callback de reject sur cette promise on délègue au defer
                    deferred.reject(state.value);
                }
            } catch (e) {
                deferred.reject(e);
            }
        });
    }

    function Promise() {
        this.$$state = {};
    }

    Promise.prototype.then = function (onFulFilled, onRejected, onProgress) {

        var result = new Deferred();

        //enregistrement du callback
        this.$$state.pending = this.$$state.pending || [];

        //We put the success callback in index 1 of the array and the rejection errback in index 2 of
        //the array. This way the indexes match the Promise status codes we’re using
        this.$$state.pending.push([result, onFulFilled, onRejected, onProgress]);

        if(this.$$state.status > 0){
            //Si la promesse a déjà été résolu et que le callback a été enregistré après,
            //on le détecte et on déclenche le callback
            scheduleProcessQueue(this.$$state);
        }

        return result.promise;
    };

    Promise.prototype.catch = function(onRejected){
        return this.then(null,onRejected);
    };

    function handleFinallyCallback(callback, value, resolved){
        var callbackValue = callback();

        if(callbackValue && callbackValue.then){
            return callbackValue.then(function() {
                return makePromise(value, resolved);
            });
        }else{
            return makePromise(value,resolved);
        }

    }

    Promise.prototype.finally = function(callback, progressBack){
        return this.then(function(value){
            return handleFinallyCallback(callback, value, true);
        },function(rejection){
            return handleFinallyCallback(callback, rejection, false);
        }, progressBack);
    };


    function Deferred() {
        this.promise = new Promise();
    }

    Deferred.prototype.resolve = function (value) {

        if(this.promise.$$state.status){
            //la promesse a déjà été résolue ou reject puisque le flag status est différent de undefined
            return;
        }

        if(value && _.isFunction(value.then)){
            //value est une promise car il possède la function then
            //il ne faut pas resolve maintenant mais uniquement quand value sera résolu
            value.then(
                _.bind(this.resolve,this),
                _.bind(this.reject,this),
                _.bind(this.notify,this)
            );

        } else{
            //stocke la valeur retournée par le defer
            this.promise.$$state.value = value;
            this.promise.$$state.status = 1;
            //schedule une tâche à exécuter lors du prochain cycle de digest afin de déclencher
            //les callback sur la promise
            scheduleProcessQueue(this.promise.$$state);
        }
    };

    Deferred.prototype.reject = function(reason){

        if(this.promise.$$state.status){
            //la promesse a déjà été résolue puisque le flag status n'est  plus undefined
            return;
        }

        this.promise.$$state.value = reason;
        this.promise.$$state.status = 2;

        scheduleProcessQueue(this.promise.$$state);

    };

    Deferred.prototype.notify = function(progress){
        var pending = this.promise.$$state.pending;

        if(pending && pending.length && !this.promise.$$state.status){
            callLater(function(){
                _.forEach(pending, function(handlers){

                    var deferred = handlers[0];
                    var progressBack = handlers[3];
                    try{
                        deferred.notify(_.isFunction(progressBack) ? progressBack(progress) : progress);
                    } catch(e){
                        console.log(e);
                    }
                });
            });
        }
    };

    function defer() {
        return new Deferred();
    }

    function reject(rejection){
        var d = defer();
        d.reject(rejection);
        return d.promise;
    }

    function when(value, callback, errback, progressback){
        var d = defer();
        d.resolve(value);
        return d.promise.then(callback, errback, progressback);
    }

    function all(promises){

        var results = _.isArray(promises) ? [] : {};
        var counter = 0;
        var d = defer();

        _.forEach(promises, function(promise, index){
            counter++;
            when(promise).then(function(value){
                results[index] = value;
                counter--;
                if(!counter){
                    d.resolve(results);
                }
            }, function(rejection){
                d.reject(rejection);
            });
        });

        if (!counter) {
            d.resolve(results);
        }

        return d.promise;
    }

    var $Q = function(resolver){
        if(!_.isFunction(resolver)){
            throw 'Expected function, got ' + resolver;
        }

        var d  = defer();

        resolver(_.bind(d.resolve, d),
            _.bind(d.reject,d));

        return d.promise;

    };

    return _.extend($Q,{
        defer: defer,
        reject : reject,
        when:when,
        all:all
    });
}


function $$QProvider(){
    this.$get = ['$rootScope', function($rootScope){
        return qFactory(function(callback){
            setTimeout(callback, 0);
        });
    }];
}


function $QProvider() {
    this.$get = ['$rootScope',function ($rootScope) {

        return qFactory(function(callback){
            $rootScope.$evalAsync(callback);
        });
    }];
}