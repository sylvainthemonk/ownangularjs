/* jshint globalstrict: true */
'use strict';

function setupModuleLoader(window) {

    //si obj[name] existe renvoie obj[name] sinon renvoie la factory
    var ensure = function (obj, name, factory) {
        return obj[name] || (obj[name] = factory());
    };

    //appeler Object() équivaut à new Object()
    var angular = ensure(window, 'angular', Object);

    var createModule = function (name, requires, modules, configFn) {

        if (name === 'hasOwnProperty') {
            throw 'hasOwnProperty is not a valid module name';
        }

        var invokeQueue = [];
        var configBlocks = [];
        var runBlocks = [];

        var invokeLater = function(service, method, arrayMethod, queue) {
            return function() {
                var item = [service, method, arguments];
                queue = queue || invokeQueue;
                queue[arrayMethod || 'push'](item);
                return moduleInstance;
            };
        };

        var moduleInstance = {
            name: name,
            requires: requires,
            constant : invokeLater('$provide','constant', 'unshift'),
            provider : invokeLater('$provide', 'provider'),
            factory : invokeLater('$provide', 'factory'),
            value : invokeLater('$provide','value'),
            service : invokeLater('$provide','service'),
            directive : invokeLater('$compileProvider', 'directive'),
            controller : invokeLater('$controllerProvider', 'register'),
            config : invokeLater('$injector', 'invoke', 'push', configBlocks),
            run : function(fn){
                moduleInstance._runBlocks.push(fn);
                return moduleInstance;
            },
            _invokeQueue : invokeQueue,
            _configBlocks : configBlocks,
            _runBlocks : runBlocks
        };

        if(configFn){
            moduleInstance.config(configFn);
        }

        modules[name] = moduleInstance;
        return moduleInstance;
    };

    var getModule = function (name, modules) {

        if (modules.hasOwnProperty(name)) {
            return modules[name];
        } else {
            throw 'Module ' + name + ' is not available!';
        }
    };

    //création de la fonction de création de module
    //singleton, si la fonction existe déjà sur l'objet window.angular alors elle
    //n'est pas recréée.
    ensure(angular, 'module', function () {

        //variable libre sur la fonction factory (créee 1 et 1 seule fois)
        //stocke tous les modules créées
        var modules = {};

        return function (name, requires, configFn) {
            if (requires) {
                return createModule(name, requires, modules, configFn);
            } else {
                return getModule(name, modules);
            }
        };
    });

}
