/* jshint globalstrict: true */
'use strict';

function $HttpProvider (){

    //array of interceptor
    var interceptorFactories = this.interceptors = [];

    var useApplyAsync = false;

    this.useApplyAsync = function(value){
        if(_.isUndefined(value)){
            return value;
        }else{
            useApplyAsync = !!value;
            return this;
        }
    };


    function isBlob(object){
        return object.toString() === '[object Blob]';
    }

    function isFile(object){
        return object.toString() === '[object File]';
    }

    function isFormData(object){
        return object.toString() === '[object FormData]';
    }

    function isJsonLike(data) {
        if (data.match(/^\{(?!\{)/)) {
            return data.match(/\}$/);
        } else if (data.match(/^\[/)) {
            return data.match(/\]$/);
        }
    }

    function defaultHttpResponseTransform(data, headers){
        if(_.isString(data)){
            var contentType = headers('Content-Type');

            if((contentType && contentType.indexOf('application/json') === 0) || isJsonLike(data)){
                return JSON.parse(data);
            }else{
                return data;
            }
        }else{
            return data;
        }
    }

    var defaults = this.defaults = {
        headers : {
            common : {
                Accept : 'application/json, text/plain, */*'
            },
            post: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            put: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            patch: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        },
        transformRequest : [function(data){
            if(_.isObject(data) && !isBlob(data)
                && !isFile(data) && !isFormData(data)){
                //serialization en json
                return JSON.stringify(data);
            }else{
                return data;
            }
        }],
        transformResponse : [defaultHttpResponseTransform]
    };

    this.$get = ['$httpBackend', '$q', '$rootScope','$injector', function($httpBackend, $q, $rootScope, $injector){

        var interceptors = _.map(interceptorFactories, function(fn){

            return _.isString(fn)? $injector.get(fn) : $injector.invoke(fn);

            //utilisation de l'invoke du $injector pour l'injection de dépendance au niveau
            //des factories function d'interceptor

        });

        function isSuccess(status){
            return status >= 200 && status<300;
        }

        /**
         * fonction de parsing des headers de reponse
         * @param headers
         */
        function parseHeaders(headers){

            if(_.isObject(headers)){
                //normalisation des headers s'il s'agit déjà d'un objet (cas des request headers)
                return _.transform(headers, function(result,v,k){
                    result[_.trim(k.toLowerCase())] = _.trim(v);
                }, {});
            }else{
                //response headers
                var lines = headers.split('\n');

                return _.transform(lines, function(result, line){
                    var separatorAt = line.indexOf(':');
                    var name = _.trim(line.substring(0, separatorAt)).toLowerCase();
                    var value = _.trim(line.substring(separatorAt + 1));

                    if(name){
                        result[name] = value;
                    }
                }, {});
            }
        }

        function headersGetter(headers){
            //objet qui contiendra le résultat du parsing de la headersString
            var headersObj;

            return function(name){

                headersObj = headersObj || parseHeaders(headers);

                if(name){
                    return headersObj[name.toLowerCase()];
                }else{
                    return headersObj;
                }
            };
        }

        function executeHeaderFns(headers, config) {
            return _.transform(headers, function(result, v, k) {
                if (_.isFunction(v)) {
                    v = v(config);
                    if (_.isNull(v) || _.isUndefined(v)) {
                        delete result[k];
                    } else {
                        result[k] = v;
                    }
                }
            }, headers);
        }

        function mergeHeaders(config){

            //request headers setté par l'utilisateur dans l'objet de conf
            var reqHeaders = _.extend(
                {},
                config.headers
            );

            //default headers
            var defHeaders = _.extend(
                {},
                defaults.headers.common,
                defaults.headers[(config.method || 'get').toLowerCase()]
            );

            _.forEach(defHeaders, function(value, key){
                var headerExists = _.any(reqHeaders, function(v, k){
                    return k.toLowerCase() === key.toLowerCase();
                });

                if(!headerExists){
                    reqHeaders[key] = value;
                }
            });

            return executeHeaderFns(reqHeaders, config);
        }

        function transformData(data, headers, status, transform) {
            if (_.isFunction(transform)) {
                return transform(data, headers, status);
            } else {
                return _.reduce(transform, function(data, fn) {
                    return fn(data, headers, status);
                }, data);
            }
        }

        /**
         * Send the request
         *
         * @param config
         * @param reqData
         * @returns {*}
         */
        function sendReq(config, reqData){
            var deferred = $q.defer();

            $http.pendingRequests.push(config);

            function done(status, response, headersString, statusText){

                status = Math.max(status,0);

                function resolvePromise(){
                    deferred[isSuccess(status)? 'resolve' : 'reject']({
                        status : status,
                        statusText : statusText,
                        data : response,
                        headers : headersGetter(headersString),
                        config: config
                    });
                }

                if(useApplyAsync){
                    //optim applyaync pour éviter de lancer un apply à chaque response
                    $rootScope.$applyAsync(resolvePromise);
                }else{
                    resolvePromise();

                    if(!$rootScope.$$phase){
                        $rootScope.$apply();
                    }
                }
            }

            function buildUrl(url, params) {

                _.forEach(params, function (value, key) {

                    if(_.isNull(value) || _.isUndefined(value)){
                        return;
                    }

                    if(!_.isArray(value)){
                        value = [value];
                    }

                    _.forEach(value, function(v){

                        if(_.isObject(v)){
                            v = JSON.stringify(v);
                        }

                        url += url.indexOf('?') === -1 ? '?' : '&';
                        url += encodeURIComponent(key) + '=' + encodeURIComponent(v);
                    });
                });

                return url;
            }

            var url = buildUrl(config.url, config.params);

            //l'envoi de la request se fait dans le http backend
            $httpBackend(
                config.method,
                url,
                reqData,
                done,
                config.headers,
                config.timeout,
                config.withCredentials
            );

            deferred.promise.then(function(){
                _.remove($http.pendingRequests, config);
            }, function(){
                _.remove($http.pendingRequests, config);
            });

            return deferred.promise;
        }

        /**
         * $http function
         * @param requestConfig
         * @returns {*}
         */
        function $http(requestConfig){

            //prepare the request

            var config = _.extend({
                method : 'GET',
                transformRequest : defaults.transformRequest,
                transformResponse : defaults.transformResponse

            }, requestConfig);

            config.headers = mergeHeaders(requestConfig);

            var promise = $q.when(config);

            _.forEach(interceptors, function(interceptor){
                //exécution de la méthode request de l'objet interceptor une fois que la config est prête
                promise = promise.then(interceptor.request, interceptor.requestError);
            });

            //le resolve callback sera appelé avec l'objet config
            promise = promise.then(serverRequest);

            //à partir de là les then callback seront appelés une fois la réponse reçue
            _.forEachRight(interceptors, function(interceptor){
                promise = promise.then(interceptor.response, interceptor.responseError)
            });

            //ajout des convenient method
            promise.success = function(fn){
                promise.then(function(response){
                    fn(response.data, response.status, response.headers, config);
                });
                return promise; //c'est pour ça qu'on ne peut pas faire du .success sur la fonction de call back
            };

            promise.error = function(fn) {
                promise.catch(function(response) {
                    fn(response.data, response.status, response.headers, config);
                });
                return promise;
            };

            return promise;
        };

        function serverRequest(config){

            if(_.isUndefined(config.withCredentials)
                && !_.isUndefined(defaults.withCredentials)){
                config.withCredentials = defaults.withCredentials;
            }

            var reqData = transformData(config.data ,
                headersGetter(config.headers), undefined,
                config.transformRequest)

            if(_.isUndefined(reqData)){
                _.forEach(config.headers, function(v, k){
                    if(k.toLowerCase()=== 'content-type'){
                        delete config.headers[k];
                    }
                });
            }

            function transformResponse(response){

                if(response.data){
                    response.data = transformData(
                        response.data,
                        response.headers,
                        response.status,
                        config.transformResponse);
                }

                if(isSuccess(response.status)){
                    return response;
                }else {
                    return $q.reject(response);
                }
            }

            return sendReq(config, reqData).then(transformResponse, transformResponse);

        }

        $http.defaults = defaults;
        $http.pendingRequests = [];

        _.forEach(['get', 'head', 'delete'], function(method){
            $http[method] = function(url, config) {
                return $http(_.extend(config || {}, {
                    method: method.toUpperCase(),
                    url: url
                }));
            }
        });

        _.forEach(['post', 'put', 'patch'], function(method) {
            $http[method] = function(url, data, config) {
                return $http(_.extend(config || {}, {
                    method: method.toUpperCase(),
                    url: url,
                    data: data
                }));
            };
        });

        return $http;

    }];
}